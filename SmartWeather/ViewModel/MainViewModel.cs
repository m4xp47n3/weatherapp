using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using SmartWeather.Model;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;

namespace SmartWeather.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        /// <summary>
        /// The <see cref="DayList" /> property's name.
        /// </summary>
        public const string DayListPropertyName = "DayList";

        private ObservableCollection<Day> dayList = null;

        /// <summary>
        /// Sets and gets the DayList property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ObservableCollection<Day> DayList
        {
            get
            {
                return dayList;
            }

            set
            {
                if (dayList == value)
                {
                    return;
                }

                RaisePropertyChanging(DayListPropertyName);
                dayList = value;
                RaisePropertyChanged(DayListPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="CurrentDay" /> property's name.
        /// </summary>
        public const string CurrentDayPropertyName = "CurrentDay";

        private Day _currentDay = null;

        /// <summary>
        /// Sets and gets the CurrentDay property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Day CurrentDay
        {
            get
            {
                return _currentDay;
            }

            set
            {
                if (_currentDay == value)
                {
                    return;
                }

                RaisePropertyChanging(CurrentDayPropertyName);
                _currentDay = value;
                RaisePropertyChanged(CurrentDayPropertyName);
            }
        }

        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                DayList = new ObservableCollection<Day>();
                DayList.Add(new Day { temp = { day = 20, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now });
                DayList.Add(new Day { temp = { day = 28, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(1) });
                DayList.Add(new Day { temp = { day = 21, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(2) });
                DayList.Add(new Day { temp = { day = 22, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(3) });
                DayList.Add(new Day { temp = { day = 23, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(4) });
                DayList.Add(new Day { temp = { day = 24, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(5) });
                DayList.Add(new Day { temp = { day = 25, eve = 18, morn = 19, night = 16 }, Time = DateTime.Now.AddDays(6) });
                CurrentDay = DayList[0];
            }
            else
            {
                // Code runs "for real"
                WebClient client = new WebClient();
                client.DownloadStringCompleted += (s, e) =>
                {
                    if (e.Error == null)
                    {
                        RootObject result = JsonConvert.DeserializeObject<RootObject>(e.Result);
                        DayList = new ObservableCollection<Day>(result.list);
                        CurrentDay = DayList[0];
                    }
                    else
                    {
                        MessageBox.Show("Sorry, try again");
                    }
                };
                client.DownloadStringAsync(new Uri("http://api.openweathermap.org/data/2.5/forecast/daily?q=san_jose,us&units=metric&cnt=7"));
            }
        }
    }
}